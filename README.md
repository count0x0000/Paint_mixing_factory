Paint mixing factory

This installation was built by my dad and myself to contribute to the education of the students of the The Hague University Of Applied Sciences. I was taking there a bachelor of ICT, and during one of the semesters the group should work at programming the PLC's who were driving a model paint factory. The factory was actually more a set of transport devices where plastic cups where filled from a container containing colored water.

This is where dad and I came in. We got the order to construct the hearth of the factory where a batch of 'true' paint can be mixed from 3 pigments. The batch should be pumped off to one of the four auxiliary stations where the filling of cups take place. The model should be student proof, not to easy to drive with a PLC and equipped with manual overrides for all the pumps and valves. We also wrote a 'service manual' so the students could figure out the workings, without taking the machine apart.

The service manual and demonstration movie are in Dutch though. Three Atmel micro controllers are used in this project. They are described in the service manual, but if you like to see the AVR Studio project files, you can find them here. 